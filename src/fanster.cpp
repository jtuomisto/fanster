#include "fanster.hpp"


void fanster::signal_handler(int signal)
{
    fanster::program.error.store(signal);
}


bool fanster::failure(int error)
{
    fanster::signal_handler(error);
    return false;
}


bool fanster::find_hwmon_paths()
{
    int id = -1;
    char* path_buffer;
    std::string device = PLATFORM_DEVICE_NAME;

    auto get_file_path = [](int id, const char* file) -> std::string
    {
        char path[_PATH_BUFFER_SIZE];
        snprintf(path, _PATH_BUFFER_SIZE, _HWMON_ENTRY_FORMAT, id, file);
        return path;
    };

    for(int i = 0; i < _HWMON_SEARCH; ++i)
    {
        path_buffer = realpath(get_file_path(i, _HWMON_DEVICE_SYMLINK).c_str(), NULL);

        if(path_buffer == NULL)
            continue;

        std::string resolved(path_buffer);
        free(path_buffer);

        if(resolved.substr(resolved.length() - device.length()) == device)
        {
            id = i;
            break;
        }
    }

    if(id >= 0)
    {
        fanster::path.temp_input = get_file_path(id, TEMP_INPUT_FILE);
        fanster::path.pwm_input  = get_file_path(id, PWM_INPUT_FILE);
        fanster::path.pwm_enable = get_file_path(id, PWM_ENABLE_FILE);

        #if defined(VERBOSE) || defined(DEBUG)
        std::cout << "temp input path: " << fanster::path.temp_input << "\n";
        std::cout << "pwm input path:  " << fanster::path.pwm_input  << "\n";
        std::cout << "pwm enable path: " << fanster::path.pwm_enable << "\n";
        #endif // VERBOSE || DEBUG

        return true;
    }

    return false;
}


bool fanster::test_permissions()
{
    std::ios_base::openmode mode_read  = std::ios_base::in;
    std::ios_base::openmode mode_write = mode_read | std::ios_base::out;
    bool perms = true;

    auto check = [](const std::string& path, std::ios_base::openmode mode) -> bool
    {
        return std::fstream(path, mode).good();
    };

    perms &= check(fanster::path.temp_input, mode_read);
    perms &= check(fanster::path.pwm_input,  mode_write);
    perms &= check(fanster::path.pwm_enable, mode_write);

    return perms;
}


bool fanster::load_original()
{
    bool success = true;

    auto load = [](const std::string& path, int& value) -> bool
    {
        std::ifstream handle(path);

        if(handle.good())
        {
            handle >> value;
            return !handle.fail();
        }

        return false;
    };

    success &= load(fanster::path.pwm_input, fanster::original.pwm_input);
    success &= load(fanster::path.pwm_enable, fanster::original.pwm_enable);
    success &= fanster::original.pwm_input >= 0 &&
               fanster::original.pwm_enable >= 0;

    #ifdef DEBUG
    std::cout << "Got pwm input:  " << fanster::original.pwm_input  << "\n";
    std::cout << "Got pwm enable: " << fanster::original.pwm_enable << "\n";
    #endif // DEBUG

    return success;
}


bool fanster::restore_original() {
    bool success = true;

    auto restore = [](const std::string& path, int value) -> bool
    {
        std::ofstream handle(path);

        if(handle.good())
        {
            handle << value;
            return !handle.fail();
        }

        return false;
    };

    #ifdef DEBUG
    std::cout << "Restoring pwm input:  " << fanster::original.pwm_input  << "\n";
    std::cout << "Restoring pwm enable: " << fanster::original.pwm_enable << "\n";
    #endif // DEBUG

    success &= restore(fanster::path.pwm_input, fanster::original.pwm_input);
    success &= restore(fanster::path.pwm_enable, fanster::original.pwm_enable);

    return success;
}


bool fanster::set_manual_pwm_enable()
{
    std::ofstream handle(fanster::path.pwm_enable);

    if(handle.good())
        handle << _PWM_ENABLE_MANUAL;

    return !handle.fail();
}


int fanster::calculate_fan_speed(int temp)
{
    static fanster::bezier_interpolation<float> interp(FAN_MIN, FAN_MAX, BEZIER_VALUE);

    float t = std::clamp(float(temp - TEMP_MIN) / float(_TEMP_DELTA), 0.0f, 1.0f);
    int fan_speed = interp.get_value(t);

    #if FAN_DIVISOR > 1
    fan_speed = int(std::round(float(fan_speed) / FAN_DIVISOR)) * FAN_DIVISOR;
    #endif

    return std::clamp(fan_speed, FAN_MIN, FAN_MAX);
}


bool fanster::update_temperature()
{
    static std::ifstream handle(fanster::path.temp_input);
    static fanster::moving_average<int, AVERAGE_SAMPLES> moving_avg;
    static fanster::gradient<GRADIENT_SAMPLES> gradient;

    handle.seekg(0);

    if(handle.good())
    {
        int temp = 0;
        handle >> temp;
        temp /= _TEMP_DIVISOR;

        if(temp < INCORRECT_LESS_THAN || temp > INCORRECT_MORE_THAN)
            return fanster::failure(_TEMP_MAYBE_WRONG);

        moving_avg += temp;
        gradient += (int)moving_avg;
    }
    else
    {
        return fanster::failure(_FAILED_TO_READ_TEMP);
    }

    fanster::info.temperature.store((int)moving_avg);
    fanster::info.gradient.store((double)gradient);

    return true;
}


void fanster::temperature_update_loop()
{
    auto interval_clock = fanster::clock_t(INTERVAL_MAIN);

    for(;;)
    {
        if(fanster::program.error.load() != 0 || !fanster::update_temperature())
            break;

        std::this_thread::sleep_for(interval_clock);
    }
}


void fanster::control_loop() {
    int last_change_temperature = 0;
    int last_change_fan_speed = -1;
    auto interval_clock = fanster::clock_t(INTERVAL_FAN);
    auto speed_down_clock = fanster::clock_t(SPEED_DOWN);
    auto speed_up_time = fanster::clock::now();
    auto speed_change_time = fanster::clock::now();

    int temperature = 0;
    int fan_speed = FAN_MIN;
    double gradient = 0;
    bool update = true;

    #ifndef TEST_MODE
    std::ofstream handle(fanster::path.pwm_input);
    #endif // TEST_MODE

    for(;;)
    {
        if(fanster::program.error.load() != 0) break;

        temperature = fanster::info.temperature.load();
        gradient = fanster::info.gradient.load();

        if(std::abs(temperature - last_change_temperature) >= HYSTERESIS)
        {
            fan_speed = fanster::calculate_fan_speed(temperature);

            // Too complicated?
            update = fan_speed != last_change_fan_speed &&
                     ((gradient > 0 && fan_speed > last_change_fan_speed) ||
                     ((gradient <= 0 && fan_speed < last_change_fan_speed) &&
                     (TIME_DELTA(speed_up_time) >= speed_down_clock)));

            if(update)
            {
                #ifndef TEST_MODE
                handle.seekp(0);

                if(handle.good())
                {
                    handle << fan_speed;
                }
                else
                {
                    fanster::failure(_FAN_PWM_CONTROL_LOST);
                    break;
                }
                #endif // TEST_MODE

                speed_change_time = fanster::clock::now();

                if(fan_speed > last_change_fan_speed)
                    speed_up_time = speed_change_time;

                last_change_fan_speed = fan_speed;
                last_change_temperature = temperature;

                #if defined(VERBOSE) || defined(DEBUG)
                std::cout << "pwm input: " << fan_speed << "\n";
                std::cout.flush();
                #endif // VERBOSE || DEBUG
            }
        }

        #ifdef DEBUG
        std::cout << std::fixed << "temperature: " << temperature << "\n";
        std::cout << std::fixed << "gradient:    " << gradient    << "\n";
        std::cout.flush();
        #endif // DEBUG

        std::this_thread::sleep_for(interval_clock);
    }
}
