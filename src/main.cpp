#include <iostream>
#include <csignal>
#include <thread>
#include <string>
#include "fanster.hpp"


int die(const std::string& msg)
{
    std::cerr << msg << "\n";
    return EXIT_FAILURE;
}


int main(int, char**)
{
    static_assert(HYSTERESIS >= 0, "HYSTERESIS must be larger than or equal to 0");
    static_assert(TEMP_MAX > TEMP_MIN, "TEMP_MAX must be larger than TEMP_MIN");
    static_assert(FAN_MAX > FAN_MIN, "FAN_MAX must be larger than FAN_MIN");
    static_assert(TEMP_MAX > 0, "TEMP_MAX must be larger than 0");
    static_assert(TEMP_MIN > 0, "TEMP_MIN must be larger than 0");
    static_assert(FAN_MIN >= 0 && FAN_MIN <= 255, "0 <= FAN_MIN <= 255");
    static_assert(FAN_MAX >= 0 && FAN_MAX <= 255, "0 <= FAN_MAX <= 255");
    static_assert(FAN_DIVISOR > 0, "FAN_DIVISOR must be larger than 0");

    fanster::program.error = EXIT_SUCCESS;
    fanster::info.temperature = 0;
    fanster::info.gradient = 0;

    if(!fanster::find_hwmon_paths())
        return die("Can't find sensor device!");

    if(!fanster::load_original())
        return die("Can't load pwm state!");

    #ifndef TEST_MODE
    if(!fanster::test_permissions())
        return die("Bad permissions (or missing files)! Maybe run as root?");

    if(!fanster::set_manual_pwm_enable())
        return die("Can't set pwm enable to manual!");
    #endif // TEST_MODE

    signal(SIGINT,  fanster::signal_handler);
    signal(SIGTERM, fanster::signal_handler);
    signal(SIGKILL, fanster::signal_handler);

    std::thread temp_thread(fanster::temperature_update_loop),
                ctrl_thread(fanster::control_loop);

    temp_thread.join();
    ctrl_thread.join();

    #ifndef TEST_MODE
    if(!fanster::restore_original())
        return die("Can't restore pwm state!");
    #endif // TEST_MODE

    return fanster::program.error.load();
}
