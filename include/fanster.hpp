#ifndef FANSTER_HPP_INCLUDED
#define FANSTER_HPP_INCLUDED

#include <fstream>
#include <string>
#include <chrono>
#include <atomic>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <thread>
#include <cstdio>
#include <cstdlib>
#include "config.hpp"
#include "math.hpp"


#define TIME_DELTA(x) std::chrono::duration_cast<fanster::clock_t>(fanster::clock::now() - x)


namespace fanster
{
    typedef std::chrono::milliseconds clock_t;
    typedef std::chrono::steady_clock clock;


    struct
    {
        std::atomic<int> error;
    } program;

    struct
    {
        std::string temp_input;
        std::string pwm_input;
        std::string pwm_enable;
    } path;

    struct
    {
        std::atomic<int> temperature;
        std::atomic<double> gradient;
    } info;

    struct
    {
        int pwm_input = -1;
        int pwm_enable = -1;
    } original;


    void signal_handler(int);
    bool failure(int);
    bool find_hwmon_paths();
    bool test_permissions();
    bool load_original();
    bool restore_original();
    bool set_manual_pwm_enable();
    int  calculate_fan_speed(int);
    bool update_temperature();
    void temperature_update_loop();
    void control_loop();
}

#endif // FANSTER_HPP_INCLUDED
