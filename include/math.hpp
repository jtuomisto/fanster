#ifndef MATH_HPP_INCLUDED
#define MATH_HPP_INCLUDED

#include <cmath>
#include <deque>

#define MATH_GRADIENT_THRESHOLD 0.02


namespace fanster
{
    template<class T>
    struct point
    {
        T x;
        T y;
    };

    template<class T>
    class bezier_interpolation
    {
        typedef point<T> point_t;

        private:
        point_t a; // start
        point_t b; // control
        point_t c; // end

        point_t linear_point(const point_t& a, const point_t& b, float t)
        {
            return { (b.x - a.x) * t + a.x, (b.y - a.y) * t + a.y };
        }

        public:
        bezier_interpolation(T low, T high, float bezier)
        {
            this->a.x = 0;
            this->a.y = low;
            this->b.x = 0.5;
            this->b.y = low + (high - low) * bezier;
            this->c.x = 1;
            this->c.y = high;
        }

        T get_value(float t)
        {
            point_t p1 = this->linear_point(this->a, this->b, t);
            point_t p2 = this->linear_point(this->b, this->c, t);
            point_t p3 = this->linear_point(p1, p2, t);
            return p3.y;
        }
    };

    // S = count of data points
    template<class T, int S = 5>
    class moving_average
    {
        private:
        T avg;

        public:
        moving_average() : avg(0) {}
        moving_average(T value) : avg(value) {}

        operator T()
        {
            return this->avg;
        }

        moving_average& operator=(const T& rhs)
        {
            this->avg = rhs;
            return *this;
        }

        moving_average& operator+=(const T& rhs)
        {
            this->avg = this->avg + (1.0f / S * 2) * (rhs - this->avg);
            return *this;
        }
    };

    template<int S = 5>
    class gradient
    {
        private:
        std::deque<double> data;
        moving_average<double, S> grad;

        public:
        gradient()
        {
            this->reset();
        }

        void reset()
        {
            this->data.clear();
            this->data.resize(S, 0);
        }

        operator double()
        {
            return (double)this->grad;
        }

        template<class T>
        gradient& operator+=(const T& rhs)
        {
            this->data.pop_front();
            this->data.push_back((double)rhs);
            this->grad += (this->data.back() - this->data.front()) / (S - 1);

            if(std::abs((double)this->grad) < MATH_GRADIENT_THRESHOLD)
                this->grad = 0;

            return *this;
        }
    };
}

#endif // MATH_HPP_INCLUDED
