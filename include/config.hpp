#ifndef CONFIG_HPP_INCLUDED
#define CONFIG_HPP_INCLUDED

/* Read-only mode (do everything except actually controlling a fan) */
//#define TEST_MODE

/* Debug mode, output more information */
//#define DEBUG

/* Output some information, less than with debug mode */
#define VERBOSE


/* All numbers must be integers */

/* How large the difference in temperature must be to change fan speed */
#define HYSTERESIS             3
/* Allow speeding down after this duration (milliseconds) */
#define SPEED_DOWN             12000
/* Temperature averaging */
#define AVERAGE_SAMPLES        6
/* Temperature gradient */
#define GRADIENT_SAMPLES       12
/* Bezier value for interpolation (0.5 = linear interpolation) */
#define BEZIER_VALUE           0.38

/* Fan speed update interval (milliseconds) */
#define INTERVAL_FAN           3000
/* Main temperature update interval (milliseconds) */
#define INTERVAL_MAIN          2000

/* Fan speed scales between TEMP_MIN and TEMP_MAX
   0 >= FAN_MIN < FAN_MAX <= 255 */
#define FAN_MIN                40
#define FAN_MAX                250
#define TEMP_MIN               34
#define TEMP_MAX               80
/* Fan speed is made to be divisible by this (integer) number */
#define FAN_DIVISOR            10

/* Incorrect temperature reading threshold
   If temperature reading is false for the test below, it is treated as an error
   INCORRECT_LESS_THAN >= temperature <= INCORRECT_MORE_THAN */
#define INCORRECT_LESS_THAN    10
#define INCORRECT_MORE_THAN    100

/* Platform name (as defined in /sys/devices/platform) */
#define PLATFORM_DEVICE_NAME   "asus-nb-wmi"
/* Temperature input filename (inside the directory /sys/class/hwmon/hwmon[0-9]) */
#define TEMP_INPUT_FILE        "temp1_input"
/* PWM control filename (inside the directory /sys/class/hwmon/hwmon[0-9])*/
#define PWM_INPUT_FILE         "pwm1"
/* PWM mode filename (inside the directory /sys/class/hwmon/hwmon[0-9])*/
#define PWM_ENABLE_FILE        "pwm1_enable"


/* Propably don't want to change stuff below this point */

/* HWMON entry path format */
#define _HWMON_ENTRY_FORMAT    "/sys/class/hwmon/hwmon%d/%s"
/* Symlink to /sys/devices/platform/PLATFORM_DEVICE_NAME in /sys/class/hwmon/hwmon[0-9] */
#define _HWMON_DEVICE_SYMLINK  "device"
/* How many entries (directories in /sys/class/hwmon) to search for PLATFORM_DEVICE_NAME
   32 must be enough? */
#define _HWMON_SEARCH          32
#define _PATH_BUFFER_SIZE      4096

#define _PWM_ENABLE_MANUAL     1
#define _TEMP_DELTA            (TEMP_MAX - TEMP_MIN)
#define _FAN_DELTA             (FAN_MAX - FAN_MIN)
#define _TEMP_DIVISOR          1000

/* Errors */
#define _TEMP_MAYBE_WRONG      16
#define _FAILED_TO_READ_TEMP   32
#define _FAN_PWM_CONTROL_LOST  64

#endif // CONFIG_HPP_INCLUDED
